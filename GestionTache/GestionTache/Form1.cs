﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary;
using System.IO;

namespace GestionTache
{
    public partial class Form1 : Form
    {
        List<ClassLibrary.Task> t_list;
        int pid;
        string path = @"Taskfic.txt";
        public Form1()
        {
            InitializeComponent();
            TasksFile.file_open(path);
            pid = 1;
            t_list = Task_list.create(path);
            display_list();
        }
        public void display_list()
        {
            tasks_display.Clear();
            foreach(ClassLibrary.Task tache in t_list)
            {
                tasks_display.Text = string.Format("{0} ID : {1} \r\n Title : {2} \r\n Description : {3} \r\n Deadline : {4}\r\n *********** \r\n", tasks_display.Text, tache.get_id(), tache.get_title(), tache.get_desc(), tache.get_deadline());
            }
        }
        public void reset_textboxes()
        {
            task_title_textbox.Text = "";
            task_desc_textbox.Text = "";
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if (ID_textbox.Text == null)
            {
                MessageBox.Show("Entrez un ID", "Erreur");
                return;
            }
            else
            {
                int t_id = Int32.Parse(ID_textbox.Text);
                edittask(t_id);
                MessageBox.Show(string.Format("La tâche {0} a été éditée", t_id), "Edition");
                reset_textboxes();
                TasksFile.file_write(t_list, path);
                display_list();
            }
        }

        private void TextBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label3_Click(object sender, EventArgs e)
        {

        }

        private void ToolTip1_Popup(object sender, PopupEventArgs e)
        {

        }

        private void Add_button_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(task_title_textbox.Text) | string.IsNullOrEmpty(task_desc_textbox.Text) | string.IsNullOrEmpty(dateTimePicker1.Text))
            {
                MessageBox.Show("Certains champs sont vides !", "Erreur");
                return;
            }
            else
            { 
                pid = Task_list.count(t_list)+1;
                ClassLibrary.Task tache;           
                tache = new ClassLibrary.Task(pid, task_title_textbox.Text, task_desc_textbox.Text, dateTimePicker1.Value.Date);
                t_list.Add(tache);
                MessageBox.Show(string.Format("La tâche ID#{0} a bien été ajouté.", pid), "Ajout");
                reset_textboxes();
                TasksFile.file_write(t_list, path);
                display_list();
            }
        }
        private void edittask(int pid)
        {
            foreach (ClassLibrary.Task tache in t_list)
            {
                if (pid == tache.get_id())
                {
                    if (!string.IsNullOrEmpty(task_title_textbox.Text))
                    {
                        tache.set_title(task_title_textbox.Text);
                    }
                    if (!string.IsNullOrEmpty(task_desc_textbox.Text))
                    {
                        tache.set_desc(task_desc_textbox.Text);
                    }
                }
            }
        }
        private void Delete_button_Click(object sender, EventArgs e)
        {
            if (ID_textbox.Text == null)
            {
                MessageBox.Show("Entrez un ID", "Erreur");
                return;
            }
            else
            {
                int t_id = Int32.Parse(ID_textbox.Text);
                int i;
                for (i = 0; i < t_list.Count; i++)
                {
                    if (t_list[i].get_id() == t_id)
                    {
                        t_list.RemoveAt(i);
                        MessageBox.Show(string.Format("Tâche ID#{0} supprimée.", t_id), "Suppression");
                    }
                }
                TasksFile.file_write(t_list, path);
                display_list();
            }
        }
    }
}
