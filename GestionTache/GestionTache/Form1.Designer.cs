﻿namespace GestionTache
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.tasks_display = new System.Windows.Forms.TextBox();
            this.task_title_textbox = new System.Windows.Forms.TextBox();
            this.task_desc_textbox = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.add_button = new System.Windows.Forms.Button();
            this.edit_button = new System.Windows.Forms.Button();
            this.delete_button = new System.Windows.Forms.Button();
            this.ID_textbox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tasks_display
            // 
            this.tasks_display.Location = new System.Drawing.Point(538, 73);
            this.tasks_display.Multiline = true;
            this.tasks_display.Name = "tasks_display";
            this.tasks_display.ReadOnly = true;
            this.tasks_display.Size = new System.Drawing.Size(351, 404);
            this.tasks_display.TabIndex = 0;
            this.tasks_display.TextChanged += new System.EventHandler(this.TextBox1_TextChanged);
            // 
            // task_title_textbox
            // 
            this.task_title_textbox.Location = new System.Drawing.Point(187, 121);
            this.task_title_textbox.MaxLength = 75;
            this.task_title_textbox.Name = "task_title_textbox";
            this.task_title_textbox.Size = new System.Drawing.Size(294, 20);
            this.task_title_textbox.TabIndex = 1;
            // 
            // task_desc_textbox
            // 
            this.task_desc_textbox.Location = new System.Drawing.Point(187, 164);
            this.task_desc_textbox.Multiline = true;
            this.task_desc_textbox.Name = "task_desc_textbox";
            this.task_desc_textbox.Size = new System.Drawing.Size(294, 66);
            this.task_desc_textbox.TabIndex = 2;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(187, 247);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(191, 20);
            this.dateTimePicker1.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(147, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Titre :";
            this.label1.Click += new System.EventHandler(this.Label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(115, 167);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Description :";
            this.label2.Click += new System.EventHandler(this.Label2_Click);
            // 
            // add_button
            // 
            this.add_button.Location = new System.Drawing.Point(187, 300);
            this.add_button.Name = "add_button";
            this.add_button.Size = new System.Drawing.Size(90, 23);
            this.add_button.TabIndex = 7;
            this.add_button.Text = "Ajouter";
            this.add_button.UseVisualStyleBackColor = true;
            this.add_button.Click += new System.EventHandler(this.Add_button_Click);
            // 
            // edit_button
            // 
            this.edit_button.Location = new System.Drawing.Point(283, 300);
            this.edit_button.Name = "edit_button";
            this.edit_button.Size = new System.Drawing.Size(90, 23);
            this.edit_button.TabIndex = 8;
            this.edit_button.Text = "Modifier";
            this.edit_button.UseVisualStyleBackColor = true;
            this.edit_button.Click += new System.EventHandler(this.Button2_Click);
            // 
            // delete_button
            // 
            this.delete_button.Location = new System.Drawing.Point(379, 300);
            this.delete_button.Name = "delete_button";
            this.delete_button.Size = new System.Drawing.Size(90, 23);
            this.delete_button.TabIndex = 9;
            this.delete_button.Text = "Supprimer";
            this.delete_button.UseVisualStyleBackColor = true;
            this.delete_button.Click += new System.EventHandler(this.Delete_button_Click);
            // 
            // ID_textbox
            // 
            this.ID_textbox.Location = new System.Drawing.Point(319, 329);
            this.ID_textbox.Name = "ID_textbox";
            this.ID_textbox.Size = new System.Drawing.Size(37, 20);
            this.ID_textbox.TabIndex = 10;
            this.ID_textbox.TextChanged += new System.EventHandler(this.TextBox4_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(289, 332);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "ID :";
            this.label3.Click += new System.EventHandler(this.Label3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(901, 506);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ID_textbox);
            this.Controls.Add(this.delete_button);
            this.Controls.Add(this.edit_button);
            this.Controls.Add(this.add_button);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.task_desc_textbox);
            this.Controls.Add(this.task_title_textbox);
            this.Controls.Add(this.tasks_display);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tasks_display;
        private System.Windows.Forms.TextBox task_title_textbox;
        private System.Windows.Forms.TextBox task_desc_textbox;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button add_button;
        private System.Windows.Forms.Button edit_button;
        private System.Windows.Forms.Button delete_button;
        private System.Windows.Forms.TextBox ID_textbox;
        private System.Windows.Forms.Label label3;
    }
}

