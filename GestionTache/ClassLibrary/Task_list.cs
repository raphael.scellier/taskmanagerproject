﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ClassLibrary
{
    public class Task_list
    {
        public static List<Task> create(string path)
        {
            List<Task> t_list = new List<Task>();
            string[] lines = File.ReadAllLines(path);
            foreach (string line in lines)
            {

                string[] ligne = line.Split(';');
                int a;
                bool success = Int32.TryParse(ligne[0], out a);
                DateTime deadline = Convert.ToDateTime(ligne[3]);
                if (success)
                {
                    Task tmp = new Task(a, ligne[1], ligne[2], deadline);
                    t_list.Add(tmp);
                }
            }
            return t_list;
        }
        public static int count(List<Task> t_list)
        {
            int id=0;
            foreach (Task tache in t_list)
            {
                id = id + 1;
            }
            return id;
        }
    }
}
