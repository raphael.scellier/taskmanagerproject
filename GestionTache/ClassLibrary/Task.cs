﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Task
    {
        private int ID;
        private string title;
        private string desc;
        private DateTime deadline;

        public Task(int t_id, string t_title, string t_desc, DateTime t_deadline)
        {
            ID = t_id;
            title = t_title;
            desc = t_desc;
            deadline = t_deadline;
        }
        public int get_id()
        {
            return ID;
        }
        public string get_title()
        {
            return title;
        }
        public string get_desc()
        {
            return desc;
        }
        public DateTime get_deadline()
        {
            return deadline;
        }
        public void set_id(int t_id)
        {
            this.ID = t_id;
        }
        public void set_title(string t_title)
        {
            this.title = t_title;
        }
        public void set_desc(string t_desc)
        {
            this.desc = t_desc;
        }
        public void set_deadline(DateTime t_deadline)
        {
            this.deadline = t_deadline;
        }
    }
}
