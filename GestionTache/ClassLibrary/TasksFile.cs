﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ClassLibrary
{
    public class TasksFile
    {
        public static void file_open(string path)
        {
            if (!File.Exists(path))
            {
                File.WriteAllText(path, "");
            }
        }

        public static void file_write(List<Task> t_list, string path)
        { 
            File.WriteAllText(path, "");
            foreach (Task tache in t_list)
            {
                string toutelatache = string.Format("{0};{1};{2};{3}", tache.get_id(), tache.get_title(), tache.get_desc(), tache.get_deadline());
                using (StreamWriter file = new StreamWriter(path, true))
                {
                    file.WriteLine(toutelatache);
                }
            }
        }
    }
}
